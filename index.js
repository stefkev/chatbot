var express = require("express");
var jade = require("jade");
var RiveScript = require("rivescript");
var bodyParser = require("body-parser");

var app = express();

app.set('view engine', 'jade');
app.use(express.static(process.cwd() + '/public'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())



var bot = new RiveScript({utf8: true});


bot.loadDirectory('brain', loading_done, loading_error);


function loading_done(){
    console.log('Rivescript successfully loaded brain scripts')
    bot.sortReplies();
}
function loading_error(err){
    console.log('Error happened '+err)
}




app.get('/', function(req, res){
    res.render('index')
})

app.get('/chat', function(req, res){
    var msg = req.query.message;
    var reply = bot.reply('soandso', msg);
    reply = reply.replace(/\n/g, "<br>");
    res.json({reply: reply})
})


app.listen(80, function(){
    console.log('server started')
})