$(document).ready(function(){
    $('#send').on('click', function(){
        var message = $("#message").val();
        $("#message").val('');
        appendToChat('You', message)
        $.getJSON('/chat?message='+message)
            .done(function(data){
                appendToChat('Bot', data.reply)
            })

    })
})


function appendToChat(who, message){
    $('#chat').append('<label>'+who+': '+message+'</label><br/>')
}